# coding: utf-8

__author__ = 'cleardusk'

import sys
import argparse
import cv2
import yaml
import os
import glob

from FaceBoxes import FaceBoxes
from TDDFA import TDDFA
# from utils.render import render
from utils.render_ctypes import render  # faster
from utils.depth import depth
from utils.pncc import pncc
from utils.uv import uv_tex
from utils.pose import viz_pose
from utils.serialization import ser_to_ply, ser_to_obj
from utils.functions import draw_landmarks, get_suffix
from utils.tddfa_util import str2bool
from utils.tddfa_util import _parse_param

from FaceBoxes.FaceBoxes_ONNX import FaceBoxes_ONNX
from TDDFA_ONNX import TDDFA_ONNX

import numpy as np


if(len(sys.argv) < 2):
    print("ERROR: please input files path")
    print("python.exe lumio.py [PATH]")
    exit(1)

cfg = yaml.load(open('configs/mb1_120x120.yml'), Loader=yaml.SafeLoader)

# Init FaceBoxes and TDDFA, recommend using onnx flag
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
os.environ['OMP_NUM_THREADS'] = '4'

face_boxes = FaceBoxes_ONNX()
tddfa = TDDFA_ONNX(**cfg)

filelist = glob.glob(os.path.join(sys.argv[1], 'images\\*.jpg'))
filelist.extend(glob.glob(os.path.join(sys.argv[1], 'images\\*.png')))

for filepath in filelist:
    print("load: ", filepath)
    # Given a still image path and load to BGR channel
    img = cv2.imread(filepath)

    # Detect faces, get 3DMM params and roi boxes
    boxes = face_boxes(img)
    n = len(boxes)
    if n == 0:
        print(f'No face detected, exit')
        sys.exit(-1)
    print(f'Detect {n} faces')

    param_lst, roi_box_lst = tddfa(img, boxes)

    ver_lst = tddfa.recon_vers(param_lst, roi_box_lst, dense_flag=False)

    ########################################################
    ## IF YOU CHANGE THE INPUT FORMAT THIS MIGHT BE BROKEN #
    ########################################################
    filename = os.path.basename(filepath).split(get_suffix(filepath))[0]
    filename = filename.split('_')[-1].lower()
    ########################################################

    outpath = os.path.join(sys.argv[1], 'params/')
    if not os.path.exists(outpath):
        os.makedirs(outpath)

    R = np.concatenate((param_lst[0][:3], param_lst[0][4:7], param_lst[0][8:11]))
    offset = np.array([param_lst[0][3], param_lst[0][7], param_lst[0][11]])
    params = np.concatenate((param_lst[0][12: ], R, offset))
    np.savetxt(outpath + filename + ".txt" , params)

    outpath = os.path.join(sys.argv[1], 'landmarks/')
    if not os.path.exists(outpath):
        os.makedirs(outpath)
    np.savetxt(outpath + filename + ".txt" , ver_lst[0])

