#ifndef LANDMARKS_H
#define LANDMARKS_H

#include <vector>
#include <AGFX/AGFX.h>

#define GLOG_NO_ABBREVIATED_SEVERITIES
#include <ceres/ceres.h>
#include <glog/logging.h>

class Landmarks
{
public:
	struct BaseParams
	{
	Eigen::MatrixXd base;
	Eigen::MatrixXd A0;
	Eigen::MatrixXd A1;
	
	public:
		BaseParams(bool isDense)
		{
			dense = isDense;
			if (isDense) 
			{
				base.resize(115095, 1);
				A0.resize(115095, 40);
				A1.resize(115095, 10);
			}
			else
			{
				base.resize(204, 1);
				A0.resize(204, 40);
				A1.resize(204, 10);
			}
		
		}
	
		bool isDense() { return dense; }

	private:
		bool dense;

	};
	struct Params
	{
	public:
		
		Params() :
			params(62)
		{
			setZero();
		}
		Params(std::vector<double> params) :
			params(62)
		{
			setParams(params);
		}
		Params(double* params) :
			params(62)
		{
			setParams(params);
		}

		void print()
		{
			std::cout << "===========================================\n";
			std::cout << "alpha 0 : ";
			for (std::vector<double>::const_iterator i = params.begin(); i != params.begin() + 40; ++i)
				std::cout << *i << ' ';
			std::cout << "\nalpha 1 : ";
			for (std::vector<double>::const_iterator i = params.begin() + 40; i != params.begin() + 50; ++i)
				std::cout << *i << ' ';
			std::cout << "\nR : ";
			for (std::vector<double>::const_iterator i = params.begin() + 50; i != params.begin() + 59; ++i)
				std::cout << *i << ' ';
			std::cout << "\nOffset : ";
			for (std::vector<double>::const_iterator i = params.begin() + 59; i != params.end(); ++i)
				std::cout << *i << ' ';
			std::cout << "\n===========================================\n";
		}

		// setter
		void setParams(std::vector<double> params)
		{
			this->params = params;
		}
		void setParams(double* params)
		{
			std::copy(&params[0], &params[62], this->params.begin());
		}
		void setZero()
		{
			std::fill(params.begin(), params.end(), 0);
			params[50] = params[54] = params[58] = 1;
		}

		// getter
		double* getParams()
		{
			return params.data();
		}
		Eigen::MatrixXd getAlpha0() 
		{
			return Map<const Eigen::MatrixXd>(&params[0], 40, 1);
		}
		Eigen::MatrixXd getAlpha1()
		{
			return Map<const Eigen::MatrixXd>(&params[40], 10, 1);
		}
		Eigen::MatrixXd getR()
		{
			return Map<const Eigen::MatrixXd, RowMajor>(&params[50], 3, 3);
		}
		Eigen::MatrixXd getOffsetxX(bool isDense = false)
		{
			Eigen::MatrixXd offset;
			if(isDense)
				offset.resize(3, 38365);
			else
				offset.resize(3, 68);
			offset.row(0).setConstant(params[59]);
			offset.row(1).setConstant(params[60]);
			offset.row(2).setConstant(params[61]);
			return offset;
		}
		Eigen::MatrixXd getOffset()
		{
			return Map<const Eigen::MatrixXd>(&params[59], 3, 1);
		}

	private:
		std::vector<double> params;
	};
	struct LandmarksInfo
	{
	public:
		AGFX::Camera<double> camera;
		Eigen::MatrixXd landmarks;
		Params params;

		LandmarksInfo() :
			landmarks(3, 68),
			rays(68),
			cameraIndex(-1)
		{
		}

		//setter
		void setIndex(int index)
		{
			cameraIndex = index;
		}

		//getter
		int getIndex()
		{
			return cameraIndex;
		}
		AGFX::Rayd getRay(int index, int height = 1842, int width = 2432)
		{
			AGFX::Vec2d pix(landmarks(0, index), landmarks(1, index));
			if (cameraIndex < 3)
			{
				pix = pix.Rotate270();
				pix.Set(pix.x, pix.y + height);
			}
			else
			{
				pix = pix.Rotate90();
				pix.Set(pix.x + width, pix.y);
			}
			return camera.PixelToWorldRay(pix);
		}

	private:
		int cameraIndex;
		std::vector<AGFX::Rayd> rays;
	};

	static bool loadBaseParams(
		std::string paramsPath,
		Landmarks::BaseParams &base);

	static bool loadParams(
		std::string paramsPath,
		Params &params);

	static bool loadLandmarks(std::string landmarksPath, LandmarksInfo &landmarksInfo);

	static void optimizeParams(Landmarks::BaseParams baseParams, Landmarks::Params &params, Eigen::MatrixXd u_target, bool parameterizeTransform = false, bool printError = false);

	static void optimizeTransformation(double *params, Eigen::MatrixXd source, Eigen::MatrixXd target, bool printError = false);

	static bool saveParams(string outputPath,
		Params &params,
		string prefix = "");

	static Eigen::MatrixXd createMorphedPoints(Params &params,
		Landmarks::BaseParams baseParams,
		bool transform,
		string outPath = "");

	static Eigen::MatrixXd createMorphedPoints(Params &params,
		Landmarks::BaseParams baseParams,
		string outPath = "");

	static bool savePointCloudPLY(const Eigen::MatrixX3d & V, const std::string filepath);

private:
	struct CostFunctor {
		explicit CostFunctor(Eigen::MatrixXd u_target, Eigen::MatrixXd u_base, Eigen::MatrixXd A0, Eigen::MatrixXd A1) :
			u_target(u_target),
			u_base(u_base),
			A0(A0),
			A1(A1)
		{ }

		bool operator()(const double * const params, double* residual) const {

			// params
			Map<const Eigen::MatrixXd> alpha0(params, 40, 1);
			Map<const Eigen::MatrixXd> alpha1(&params[40], 10, 1);
			Map<const Eigen::MatrixXd, RowMajor> R(&params[50], 3, 3);
			Eigen::MatrixXd offset(3, 68);
			offset.row(0).setConstant(params[59]);
			offset.row(1).setConstant(params[60]);
			offset.row(2).setConstant(params[61]);

			Eigen::MatrixXd morph = u_base + A0 * alpha0 + A1 * alpha1;
			Map<Eigen::MatrixXd> morph_r(morph.data(), 3, 68);

			Eigen::MatrixXd error = u_target - (R * morph_r + offset);

			for (int i = 0; i < 68; i++)
			{
				for (int j = 0; j < 3; j++) residual[j + i * 3] = error(j, i);
			}

			return true;
		}

	private:
		const Eigen::MatrixXd u_target;
		const Eigen::MatrixXd u_base;
		const Eigen::MatrixXd A0;
		const Eigen::MatrixXd A1;
	};

	struct ICPCostFunctor {
		explicit ICPCostFunctor(Eigen::MatrixXd source, Eigen::MatrixXd target) :
			source(source),
			target(target)
		{ }

		bool operator()(const double * const params, double* residual) const {

			// params
			Map<const Eigen::MatrixXd, RowMajor> R(params, 3, 3);
			Eigen::MatrixXd offset(3, source.cols());
			offset.row(0).setConstant(params[9]);
			offset.row(1).setConstant(params[10]);
			offset.row(2).setConstant(params[11]);

			Eigen::MatrixXd error = target - (R * source + offset);

			for (int i = 0; i < source.cols(); i++)
			{
				for (int j = 0; j < 3; j++) residual[j + i * 3] = error(j, i);
			}

			return true;
		}

	private:
		const Eigen::MatrixXd source;
		const Eigen::MatrixXd target;
	};
	
};

#endif