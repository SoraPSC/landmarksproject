#include "Landmarks.h"

bool Landmarks::savePointCloudPLY(const Eigen::MatrixX3d & V, const std::string filepath)
{
	if ((V.rows() == 0) || (filepath.empty()) || (AGFX::stlplus::extension_part(filepath).compare("ply") != 0))
		return false;

	if (V.rows() == 0)
		return false;

	const bool has_color = false;
	const bool has_normal = false;

	const std::string folder = AGFX::stlplus::folder_part(filepath);
	if (!AGFX::stlplus::folder_exists(folder) && !AGFX::stlplus::folder_create_recur(folder))
		return false;

	std::ofstream fout(filepath);
	if (!fout.is_open())
		return false;

	{
		fout << "ply" << "\n";
		fout << "format ascii 1.0" << "\n";
		fout << "element vertex " << V.rows() << "\n";
		fout << "property float x" << "\n";
		fout << "property float y" << "\n";
		fout << "property float z" << "\n";
		if (has_normal)
		{
			fout << "property float nx" << "\n";
			fout << "property float ny" << "\n";
			fout << "property float nz" << "\n";
		}
		if (has_color)
		{
			fout << "property uchar red" << "\n";
			fout << "property uchar green" << "\n";
			fout << "property uchar blue" << "\n";
		}
		fout << "end_header" << "\n";

		for (int i = 0; i < V.rows(); i++)
		{
			fout << V(i, 0) << " " << V(i, 1) << " " << V(i, 2);
			/*if (has_normal)
			{
				fout << " " << VN(i, 0);
				fout << " " << VN(i, 1);
				fout << " " << VN(i, 2);
			}
			if (has_color)
			{
				fout << " " << (int)(std::max(std::min(255, (int)(255 * C(i, 0))), 0));
				fout << " " << (int)(std::max(std::min(255, (int)(255 * C(i, 1))), 0));
				fout << " " << (int)(std::max(std::min(255, (int)(255 * C(i, 2))), 0));
			}*/
			fout << "\n";
		}
	}

	fout.close();

	return true;
}

bool Landmarks::loadBaseParams(
	std::string paramsPath,
	Landmarks::BaseParams &base)
{
	std::ifstream datafile;
	std::ostringstream baseFile, A0File, A1File;
	int numPoints;

	if (base.isDense()) {
		baseFile << paramsPath << "u_mesh.txt";
		A0File << paramsPath << "w_shp_mesh.txt";
		A1File << paramsPath << "w_exp_mesh.txt";
		numPoints = 115095;
	}
	else
	{
		baseFile << paramsPath << "u_base.txt";
		A0File << paramsPath << "w_shp_base.txt";
		A1File << paramsPath << "w_exp_base.txt";
		numPoints = 204;
	}
	/////////////////////////
	// Load Base Landmarks //
	/////////////////////////
	{
		std::cout << "NumPoints: " << numPoints << std::endl;
		std::cout << "Load base ... " << baseFile.str() << std::endl;
		datafile.open(baseFile.str());
		if (!datafile) {
			std::cout << "error: Failed to load base landmarks" << std::endl;;
			return EXIT_FAILURE;
		}
		for (int i = 0; i < numPoints; i++)
		{
			string v;
			if (!std::getline(datafile, v))
			{
				std::cout << "error: something wrong with file format" << std::endl;
				return EXIT_FAILURE;
			}
			base.base(i, 0) = stod(v);
		}
		datafile.close(); 
	}
	///////////////////////////
	// Load Base Morph Shape //
	///////////////////////////
	{
		std::cout << "Load A0 ... " << A0File.str() << std::endl;
		datafile.open(A0File.str());
		if (!datafile) {
			std::cout << "error: Failed to load base morph " << numPoints << "x40" << std::endl;;
			return EXIT_FAILURE;
		}
		for (int i = 0; i < numPoints; i++)
		{
			std::string line;
			if (!std::getline(datafile, line))
			{
				std::cout << "error: something wrong with file format" << std::endl;
				return EXIT_FAILURE;
			}
			std::istringstream v(line);
			for (int j = 0; j < 40; j++)
			{
				string u;
				if (!std::getline(v, u, ' '))
				{
					std::cout << "error: something wrong with file format" << std::endl;
					return EXIT_FAILURE;
				}
				base.A0(i, j) = stod(u);
			}
		}
		datafile.close();
	}
	/////////////////////////
	// Load Base Morph Exp //
	/////////////////////////
	{
		std::cout << "Load A1 ... " << A1File.str() << std::endl;
		datafile.open(A1File.str());
		if (!datafile) {
			std::cout << "error: Failed to base morph " << numPoints << "x10" << std::endl;;
			return EXIT_FAILURE;
		}
		for (int i = 0; i < numPoints; i++)
		{
			std::string line;
			if (!std::getline(datafile, line))
			{
				std::cout << "error: something wrong with file format" << std::endl;
				return EXIT_FAILURE;
			}
			std::istringstream v(line);
			for (int j = 0; j < 10; j++)
			{
				string u;
				if (!std::getline(v, u, ' '))
				{
					std::cout << "error: something wrong with file format" << std::endl;
					return EXIT_FAILURE;
				}
				base.A1(i, j) = stod(u);
			}
		}
		datafile.close();
	}
	return EXIT_SUCCESS;
}

bool  Landmarks::loadParams(
	std::string filePath,
	Params &params)
{
	std::ifstream datafile;
	/////////////////
	// Load Params //
	/////////////////
	std::cout << "Load Params ... " << filePath << " ... ";
	datafile.open(filePath);
	if (!datafile) {
		std::cout << "error: Failed to load alpha 40x1" << std::endl;;
		return EXIT_FAILURE;
	}
	for (int i = 0; i < 62; i++)
	{
		std::string line;
		if (!std::getline(datafile, line))
		{
			std::cout << "error: something wrong with file format" << std::endl;
			return EXIT_FAILURE;
		}

		params.getParams()[i] = stod(line);
	}
	datafile.close();
	std::cout << "done" << std::endl;
	return EXIT_SUCCESS;
}

bool Landmarks::loadLandmarks(std::string landmarksPath, LandmarksInfo &landmarksInfo)
{
	std::ifstream landmarkFile;
	std::string tmpline;

	std::cout << "Load landmarks ... " << landmarksPath << " ... ";
	landmarkFile.open(landmarksPath);
	if (!landmarkFile) {
		std::cout << "error: Failed to load landmarks" << std::endl;;
		return EXIT_FAILURE;
	}

	std::istringstream axis[3];
	for (int i = 0; i < 3; i++) {
		if (!std::getline(landmarkFile, tmpline))
		{
			std::cout << "error: something wrong with file format" << std::endl;
			return EXIT_FAILURE;
		}
		axis[i] = std::istringstream(tmpline);
	}

	std::string x, y, z;

	for (int i = 0; i < 68; i++) {
		if (!std::getline(axis[0], x, ' '))
		{
			std::cout << "error: something wrong with file format" << std::endl;
			return EXIT_FAILURE;
		}
		if (!std::getline(axis[1], y, ' '))
		{
			std::cout << "error: something wrong with file format" << std::endl;
			return EXIT_FAILURE;
		}
		if (!std::getline(axis[2], z, ' '))
		{
			std::cout << "error: something wrong with file format" << std::endl;
			return EXIT_FAILURE;
		}
		landmarksInfo.landmarks(0, i) = std::stod(x);
		landmarksInfo.landmarks(1, i) = std::stod(y);
		landmarksInfo.landmarks(2, i) = std::stod(z);
	}
	landmarkFile.close();
	std::cout << "done" << std::endl;
	return EXIT_SUCCESS;
}

void Landmarks::optimizeParams(Landmarks::BaseParams baseParams, Landmarks::Params &params, Eigen::MatrixXd u_target, bool parameterizeTransform, bool printError)
{
	std::cout << "Optimizing Parameters ... \n";
	Landmarks::Params init_params(params);

	ceres::Problem problem;
	ceres::CostFunction* cost_function =
		new ceres::NumericDiffCostFunction<CostFunctor, ceres::CENTRAL, 204, 62>(new CostFunctor(u_target, baseParams.base, baseParams.A0, baseParams.A1));

	problem.AddResidualBlock(cost_function, NULL, params.getParams());

	if (parameterizeTransform)
	{
		std::cout << "Parameterize transformation ... \n";
		ceres::SubsetParameterization *sp
			= new ceres::SubsetParameterization(62, { 50, 51, 52,
													53, 54, 55,
													56, 57, 58,
													59, 60, 61 });
		problem.SetParameterization(params.getParams(), sp);
	}
	ceres::Solver::Options options;
	options.minimizer_progress_to_stdout = true;
	ceres::Solver::Summary summary;
	ceres::Solve(options, &problem, &summary);
	std::cout << summary.BriefReport() << "\n";
	std::cout << "done" << std::endl;
	if (printError)
	{
		double error = 0;
		for (int i = 0; i < 40; i++)
		{
			//std::cout << init_params[i] << " x " << params[i] << std::endl;
			error += init_params.getParams()[i] - params.getParams()[i];

		}
		std::cout << "RMS Error " << sqrt(pow(error, 2) / 40) << std::endl;
		std::cout << "===================================\n";
		error = 0;
		for (int i = 40; i < 50; i++)
		{
			//std::cout << init_params[i] << " x " << params[i] << std::endl;
			error += init_params.getParams()[i] - params.getParams()[i];
		}
		std::cout << "RMS Error " << sqrt(pow(error, 2) / 10) << std::endl;
		std::cout << "===================================\n";
	}
	
}

void Landmarks::optimizeTransformation(double *params, Eigen::MatrixXd source, Eigen::MatrixXd target, bool printError)
{
	std::cout << "Optimizing Transformation ... \n";
	//Landmarks::Params init_params(params);

	ceres::Problem problem;
	ceres::CostFunction* cost_function =
		new ceres::NumericDiffCostFunction<ICPCostFunctor, ceres::CENTRAL, 204, 12>(new ICPCostFunctor(source, target));

	problem.AddResidualBlock(cost_function, NULL, params);

	ceres::Solver::Options options;
	options.minimizer_progress_to_stdout = true;
	ceres::Solver::Summary summary;
	ceres::Solve(options, &problem, &summary);
	std::cout << summary.BriefReport() << "\n";
	std::cout << "done" << std::endl;


	if (printError)
	{
		Map<const Eigen::MatrixXd, RowMajor> R(params, 3, 3);
		Eigen::MatrixXd offset(3, source.cols());
		offset.row(0).setConstant(params[9]);
		offset.row(1).setConstant(params[10]);
		offset.row(2).setConstant(params[11]);

		Eigen::MatrixXd error = target - (R * source + offset);
		double mean_error = error.sum() / 204;
		double rms = 0;
		for (int i = 0; i < 68; i++)
		{
			for (int j = 0; j < 3; j++)
				rms += pow(error(j, i) - mean_error, 2);
		}

		std::cout << "RMS Error " << sqrt(rms / 204) << std::endl;
		std::cout << "===================================\n";
	}
}

bool Landmarks::saveParams(string outputPath,
	Params &params,
	string prefix)
{
	std::cout << "Saving params to [" << outputPath << prefix << "] result ... ";
	std::ostringstream filename;
	filename.str("");
	filename << outputPath << prefix << "_alpha0.txt";
	std::ofstream alpha0_file(filename.str());
	if (alpha0_file.is_open())
	{
		alpha0_file << params.getAlpha0();
	}
	alpha0_file.close();

	filename.str("");
	filename << outputPath << prefix << "_alpha1.txt";
	std::ofstream alpha1_file(filename.str());
	if (alpha1_file.is_open())
	{
		alpha1_file << params.getAlpha1();
	}
	alpha1_file.close();

	filename.str("");
	filename << outputPath << prefix << "_R.txt";
	std::ofstream R_file(filename.str());
	if (R_file.is_open())
	{
		R_file << params.getR();
	}
	R_file.close();

	filename.str("");
	filename << outputPath << prefix << "_offset.txt";
	std::ofstream offset_file(filename.str());
	if (offset_file.is_open())
	{
		offset_file << params.getOffset();
	}
	offset_file.close();

	std::cout << "done" << std::endl;
	return EXIT_SUCCESS;
}

Eigen::MatrixXd Landmarks::createMorphedPoints(Params &params,
	Landmarks::BaseParams baseParams,
	bool transform,
	string outPath)
{
	std::cout << "Re-calculate landmarks using optimized params ... ";
	Eigen::MatrixXd morphed = baseParams.base + baseParams.A0 * params.getAlpha0() + baseParams.A1 * params.getAlpha1();
	int pointsNum = baseParams.base.rows() / 3;
	morphed = Map<Eigen::MatrixXd>(morphed.data(), 3, pointsNum);

	bool isDense = false;
	if (pointsNum == 38365)
		isDense = true;

	if (transform)
		morphed = params.getR() * morphed + params.getOffsetxX(isDense);
	std::cout << "done" << std::endl;

	if (outPath != "") // save point clouds
	{
		std::cout << "Saving [" << outPath << "] result ... ";
		if (savePointCloudPLY(morphed.transpose(), outPath))
			std::cout << " successful" << std::endl;
		else
			std::cout << " failed" << std::endl;
	}

	return morphed; // [3 x PointsNum]
}

Eigen::MatrixXd Landmarks::createMorphedPoints(Params &params,
	Landmarks::BaseParams baseParams,
	string outPath)
{
	return createMorphedPoints(params,
		baseParams,
		false,
		outPath);
}
