#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>

#include <AGFX/AGFX.h>
#include <AGFX/geometry/igl/readOBJ.h>
#include <AGFX/geometry/igl/readPLY.h>
#include <AGFX/geometry/igl/per_vertex_normals.h>
#include <AGFX/geometry/igl/point_mesh_squared_distance.h>
#include <AGFX/geometry/igl/ray_mesh_intersect.h>
#include <AGFX/geometry/nanort.h>

#define GLOG_NO_ABBREVIATED_SEVERITIES
#include <windows.h>
#include <ceres/ceres.h>
#include <glog/logging.h>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing.h>
#include <dlib/image_io.h>

#include "Landmarks.h"

constexpr int WIDTH = 2432;
constexpr int HEIGHT = 1842;

const string imageName = "\\images\\*Cam4.jpg";
const string dlibDat = ".\\Data\\shape_predictor_68_face_landmarks.dat";

//DO NOT ADJUST THIS IF USE RAW IMAGE
double focalLength = 1.0;//1.2;
double camDistance = 1.0;//2.2;

bool read_directory(const std::string& name, std::string &filename)
{
	std::string pattern(name);
	WIN32_FIND_DATA data;
	HANDLE hFind;
	if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
		filename = data.cFileName;
		return EXIT_SUCCESS;
	}
	else
	{
		return EXIT_FAILURE;
	}
}

bool loadCamera(std::string inputPath, AGFX::Camera<double> *cameras)
{
	for (int i = 0; i < 12; i++)
	{
		std::ostringstream filepath;

		filepath.str("");
		filepath << inputPath << "\\cameras\\camera" << std::setfill('0') << std::setw(2) << i << ".txt";
		std::cout << "Load Camera ... " << filepath.str() << " ... ";
		AGFX::LoadLumio3DCamera(&cameras[i], filepath.str());

		std::shared_ptr<AGFX::DistortedPerspectiveProjection<double>> projectionModel = cameras[i].GetDistortedPerspectiveProjection();
		projectionModel->SetFocalLength(projectionModel->GetFocalLength() * focalLength);

		cameras[i].SetCameraToWorldTranslationVector(cameras[i].GetCameraWorldPosition() * camDistance);
		std::cout << "done" << std::endl;
	}
	return EXIT_SUCCESS;
}

bool loadLandmarks(std::string inputPath, std::vector<Landmarks::LandmarksInfo> &landmarksInfos, AGFX::Camera<double> *cameras, int camNum)
{
	std::ostringstream filepath;
	for (int i = 0; i < camNum; i++)
	{
		int index = i + 1;

		std::cout << "================ CAM " << index << " ================" << std::endl;
		landmarksInfos[i].setIndex(index);
		landmarksInfos[i].camera = cameras[index];
		std::shared_ptr<AGFX::DistortedPerspectiveProjection<double>> projectionModel = landmarksInfos[i].camera.GetDistortedPerspectiveProjection();
		projectionModel->SetFocalLength(projectionModel->GetFocalLength() * focalLength);

		landmarksInfos[i].camera.SetCameraToWorldTranslationVector(landmarksInfos[i].camera.GetCameraWorldPosition() * camDistance);

		filepath.str("");
		filepath << inputPath << "\\landmarks\\" << "cam" << index << ".txt";
		if (Landmarks::loadLandmarks(filepath.str(), landmarksInfos[i]))
			return EXIT_FAILURE;

		filepath.str("");
		filepath << inputPath << "\\params\\" << "cam" << index << ".txt";
		if (Landmarks::loadParams(filepath.str(), landmarksInfos[i].params))
			return EXIT_FAILURE;
		std::cout << "========================================" << std::endl;
	}
	return EXIT_SUCCESS;
}

bool faceAdjustWithDlib(std::string inputPath, AGFX::Camera<double> *cameras, Eigen::MatrixXd &landmarks,
							std::vector<double> &out_vertices,
							std::vector<unsigned int> &out_faces,
							nanort::BVHAccel<double> &nano_out_struct)
{
	std::cout << "Get DLIB Landmarks from Frontal Image ... ";
	std::ostringstream filepath;
	dlib::full_object_detection shape;
	dlib::frontal_face_detector detector = dlib::get_frontal_face_detector();
	dlib::shape_predictor sp;
	dlib::deserialize(dlibDat) >> sp;

	dlib::array2d<dlib::rgb_pixel> img;

	string filename;
	filepath.str("");
	filepath << inputPath << imageName;
	if (read_directory(filepath.str(), filename))
	{
		std::cout << "error : mesh not found.\n";
		return EXIT_FAILURE;
	}

	filepath.str("");
	filepath << inputPath << "\\images\\" << filename;
	std::cout << filepath.str() << " ... ";

	dlib::load_image(img, filepath.str());
	std::vector<dlib::rectangle> dets = detector(img);
	std::cout << "Found: " << dets.size() << " ... ";
	if (dets.empty())
		std::cout << "ERROR NO FACE FOUND!!!\n";

	shape = sp(img, dets[0]);

	AGFX::Vec3d hitpoint;
	long hitIndex;
	for (int i = 36; i < shape.num_parts(); i++) {
		AGFX::Vec2d pix(shape.part(i).x(), shape.part(i).y());
		pix = pix.Rotate90();
		pix.Set(pix.x + WIDTH, pix.y);
		AGFX::Rayd camToLandmark = cameras[4].PixelToWorldRay(pix);
		if (AGFX::isRayHit(out_vertices, out_faces, nano_out_struct, camToLandmark, &hitpoint, &hitIndex))
		{
			landmarks(0, i) = hitpoint.x;
			landmarks(1, i) = hitpoint.y;
			landmarks(2, i) = hitpoint.z;
		}
		//dlib::draw_solid_circle(img, shape.part(i), 3, dlib::rgb_pixel(255, 0, 0));
	}

	//filepath.str("");
	//filepath << outputPath << "frontal_dlib.png";
	//dlib::save_png(img, filepath.str());

	std::cout << "done" << std::endl;
	return EXIT_SUCCESS;
}

bool faceAdjustWithProjection(
	Eigen::MatrixXd &inLandmarks,
	Eigen::MatrixXd &outLandmarks,
	const int *landmarkCam,
	AGFX::Camera<double> *cameras, 
	std::vector<double> &out_vertices,
	std::vector<unsigned int> &out_faces,
	nanort::BVHAccel<double> &nano_out_struct)
{
	AGFX::Vec3d hitpoint;
	long hitIndex;
	for (int index = 0; index < 68; index++)
	{
		if (landmarkCam[index] == -1) {
			outLandmarks.col(index) = inLandmarks.col(index);
			continue;
		}

		AGFX::Vec3d camPos = cameras[landmarkCam[index]].GetCameraWorldPosition();
		AGFX::Vec3d landmarkPos(inLandmarks(0, index), inLandmarks(1, index), inLandmarks(2, index));
		AGFX::Rayd camToLandmark(camPos, landmarkPos - camPos);

		if (AGFX::isRayHit(out_vertices, out_faces, nano_out_struct, camToLandmark, &hitpoint, &hitIndex))
		{
			outLandmarks(0, index) = hitpoint.x;
			outLandmarks(1, index) = hitpoint.y;
			outLandmarks(2, index) = hitpoint.z;
		}
		else
		{
			outLandmarks.col(index) = inLandmarks.col(index);
		}
	}
	return EXIT_SUCCESS;
}

int main(int argc, char* argv[]) {
	if (argc < 2) {
		std::cout << "ERROR: need files path\n";
		return EXIT_FAILURE;
	}
		
	std::cout << "Initializing ..." << std::endl;
	google::InitGoogleLogging(argv[0]);

	const std::string baseParamsPath = ".\\Data\\";
	const std::string inputPath = argv[1];
	const std::string outputPath = std::string(argv[1]) + "\\out\\";

	if (CreateDirectory(outputPath.c_str(), NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError())
	{
		std::cout << "Created output folder " << outputPath << "\n";
	}
	else if(ERROR_ALREADY_EXISTS != GetLastError())
	{
		std::cout << "FAIL to create output folder " << outputPath << "\n";
		return EXIT_FAILURE;
	}

	std::ostringstream filepath;
	std::string filename;

	Eigen::MatrixXd V;
	Eigen::MatrixXd VN;
	Eigen::MatrixXd TC;
	Eigen::MatrixXi F;
	Eigen::MatrixXi FN;
	Eigen::MatrixXi FTC;
	AGFX::FImage texImg;

	///////////////////////////
	//////// LOAD MESH ////////
	///////////////////////////
	{
		filepath.str("");
		filepath << inputPath << "\\mesh\\*.obj";
		if (read_directory(filepath.str(), filename))
		{
			std::cout << "error : mesh not found.\n";
			return EXIT_FAILURE;
		}

		filepath.str("");
		filepath << inputPath << "\\mesh\\" << filename;
		std::cout << "Load mesh ... " << filepath.str() << " ... ";
		if (!igl::readOBJ(filepath.str(), V, TC, VN, F, FTC, FN))
		{
			std::cout << "error : failed to load mesh file.\n";
			return EXIT_FAILURE;
		}
		std::cout << "done" << std::endl;


		filepath.str("");
		filepath << inputPath << "\\mesh\\*.jpg";
		if (read_directory(filepath.str(), filename))
		{
			filepath.str("");
			filepath << inputPath << "\\mesh\\*.png";
			if (read_directory(filepath.str(), filename))
			{
				std::cout << "error : texture not found.\n";
				return EXIT_FAILURE;
			}
		}

		filepath.str("");
		filepath << inputPath << "\\mesh\\" << filename;
		std::cout << "Load texture ... " << filepath.str() << " ... ";
		if (!texImg.Load(filepath.str()))
		{
			std::cout << "error : failed to load image.\n";
			return EXIT_FAILURE;
		}
		std::cout << "done" << std::endl;
		igl::per_vertex_normals(V, F, VN);
	}

	nanort::BVHBuildOptions<double> nano_buildoptions;
	std::vector<double> out_vertices;
	std::vector<unsigned int> out_faces;
	nanort::BVHAccel<double> nano_out_struct;
	AGFX::prepareNanoRT(V, F, nano_buildoptions, &out_vertices, &out_faces, &nano_out_struct);

	/////////////////////////////
	//////// LOAD CAMERA ////////
	/////////////////////////////

	AGFX::Camera<double> cameras[12];
	if (loadCamera(inputPath, cameras))
		return EXIT_FAILURE;

	// Create Images From Mesh //
	//	     [optional]        //
	if (0) // To get mesh images if don't use RAW
	{
		//AGFX::FImage capture;

		//filepath.str("");
		//filepath << outputPath << "cam" << std::setfill('0') << std::setw(2) << i << ".png";
		//std::cout << "Save image: " << filepath.str() << " ... ";
		//if (AGFX::RasterizeMeshTextureImage(V, VN, F, TC, FTC, texImg, cameras[i], capture))
		//{
		//	capture.SavePNGImage(filepath.str());
		//	std::cout << " successful" << std::endl;
		//}
		//else
		//	std::cout << " failed" << std::endl;
	}

	/////////////////////////////
	// LOAD PARAMS FROM NEURAL //
	/////////////////////////////
	Landmarks::BaseParams baseParams(false);
	Landmarks::BaseParams baseParamsMesh(true);

	const int camNum = 6; // We use 6 cameras [1, 2, 3, 4, 5, 6]
	std::vector<Landmarks::LandmarksInfo> landmarksInfos(camNum); // This contains landmarks[68x3], params[62], camera

	if (Landmarks::loadBaseParams(baseParamsPath, baseParams))
		return EXIT_FAILURE;
	if(Landmarks::loadBaseParams(baseParamsPath, baseParamsMesh))
		return EXIT_FAILURE;
	if (loadLandmarks(inputPath, landmarksInfos, cameras, camNum))
		return EXIT_FAILURE;

	/////////////////////////////////
	///////// AVG LANDMARKS /////////
	/////////////////////////////////
	// This create avg. landmarks in object coordinate
	Eigen::MatrixXd landmarksAvg = Eigen::MatrixXd::Zero(3, 68);

	for (int cam = 0; cam < camNum; cam++)
	{
		Eigen::MatrixXd morphed = Landmarks::createMorphedPoints(landmarksInfos[cam].params, baseParams);

		for (int i = 0; i < 48; i++) // exclude mouth [48, 67]
		{
			landmarksAvg.col(i) += morphed.col(i) / 6;
		}

		if (cam == 2 || cam == 3) // cam 3 and cam 4
		{
			for (int i = 48; i < 68; i++)
			{
				landmarksAvg.col(i) += morphed.col(i) * 0.5;
			}
		}
	}

	//////////////////////////////////////
	// Optimize Params of Avg. Ladmarks //
	//////////////////////////////////////
	Landmarks::Params paramsAvg;
	Landmarks::optimizeParams(baseParams, paramsAvg, landmarksAvg, true); // We don't want to optimize transformation here

	//////////////////////////////////
	//////////// AVG RAYS ////////////
	//////////////////////////////////
	// This create avg. landmarks in world coordinate

	Eigen::MatrixXd landmarksRays = Eigen::MatrixXd::Zero(3, 68);
	{
		int couple = 9;
		int couple_i[] = { 1, 1, 2, 2, 3, 3, 5, 5, 6 };
		int couple_j[] = { 2, 4, 3, 4, 4, 5, 4, 6, 4 };

		for (int index = 0; index < 68; index++)
		{
			//int avgNum = 1;
			// Approx. position using closest points between rays //
			for (int cindex = 0; cindex < couple; cindex++)
			{
				int cam0 = couple_i[cindex] - 1;
				int cam1 = couple_j[cindex] - 1;
				AGFX::Vec3d p0, p1, midpoint;

				AGFX::Rayd::GetClosestPointsBetweenRays(landmarksInfos[cam0].getRay(index), landmarksInfos[cam1].getRay(index), &p0, &p1);

				//double dist = sqrt(pow(p0.x + p1.x, 2) + pow(p0.y + p1.y, 2) + pow(p0.z + p1.z, 2));

				midpoint.Set((p0.x + p1.x) * 0.5, (p0.y + p1.y) * 0.5, (p0.z + p1.z) * 0.5);
				//std::cout << dist << std::endl;
				//if (dist > 100)
				//	continue;

				landmarksRays(0, index) += midpoint.x;
				landmarksRays(1, index) += midpoint.y;
				landmarksRays(2, index) += midpoint.z;
				//avgNum++;
			}
		}
		landmarksRays /= couple;
	}

	/////////////////////////////
	// Optimize Transformation //
	/////////////////////////////

	// Transform every camera
	{
		//for (int i = 0; i < camNum; i++)
		//{
		//	double *transformation = params[i].getParams() + 50;
		//	Landmarks::optimizeTransformation(transformation, landmarksAvg, landmarksInfos[i].landmarks, true);

		//	filepath.str("");
		//	filepath << outputPath << "cam" << i + 1 << "_transformed_mesh.ply";
		//	Landmarks::createMorphedPoints(paramsAvg, baseParamsMesh, true, filepath.str());

		//	std::ostringstream prefix;
		//	prefix << "cam" << i + 1;
		//	Landmarks::saveParams(outputPath, params[i], prefix.str());

		//}
	}
	//

	double *transformation = paramsAvg.getParams() + 50;
	Landmarks::optimizeTransformation(transformation, landmarksAvg, landmarksRays, true);
	Eigen::MatrixXd landmarksAvgTransformed = Landmarks::createMorphedPoints(paramsAvg, baseParams, true);

	////////////////////////////////////////////////
	// Adjust Landmarks by DLIB Frontal Landmarks //
	////////////////////////////////////////////////
	//Get Frontal Landmarks From DLIB Instead
	if (faceAdjustWithDlib(inputPath, cameras, landmarksAvgTransformed, out_vertices, out_faces, nano_out_struct))
		return EXIT_FAILURE;


	///////////////////////////////////
	// Adjust Landmarks by Projection //
	///////////////////////////////////
	//const int landmarkCam[] = { 11, 11, 11, 11, 11, 10, 10, 10, 3, //Face outline
	//						9, 9, 9, 8, 8, 8, 8, 8,
	//						6, 6, 6, 6, 6, //right eyebrow
	//						1, 1, 1, 1, 1, //left eyebrow
	//						4, 4, 4, 4, //nose
	//						4, 4, 4, 4, 4, //undernose
	//						4, 4, 4, 4, 4, 4, //right eye
	//						4, 4, 4, 4, 4, 4, //left eye
	//						4, 4, 4, 4, 4, 4, 4, //top lip
	//						3, 3, 3, 3, 3, //bottom lip
	//						4, 4, 4, 4, 4, 4, 4, 4 }; //in lip

	const int landmarkCam[] = { 11, 11, 11, 11, 11, 10, 10, 10, 3, //Face outline
						9, 9, 9, 8, 8, 8, 8, 8,
						6, 6, 6, 6, 6, //right eyebrow
						1, 1, 1, 1, 1, //left eyebrow
						4, 4, 4, 4, //nose
						4, 4, 4, 4, 4, //undernose
						-1, -1, -1, -1, -1, -1, //right eye
						-1, -1, -1, -1, -1, -1, //left eye
						-1, -1, -1, -1, -1, -1, -1, //top lip
						-1, -1, -1, -1, -1, //bottom lip
						-1, -1, -1, -1, -1, -1, -1, -1 }; //in lip

	Eigen::MatrixXd landmarksProj(3, 68);
	if (faceAdjustWithProjection(landmarksAvgTransformed, landmarksProj, landmarkCam, cameras, out_vertices, out_faces, nano_out_struct))
		return EXIT_FAILURE;

	////////////////////////////
	////// Save Landmarks //////
	////////////////////////////
	std::cout << "Saving proj result ... ";
	filepath.str("");
	filepath << outputPath << "proj_landmarks.ply";
	if (Landmarks::savePointCloudPLY(landmarksProj.transpose(), filepath.str()))
		std::cout << " successful" << std::endl;
	else
		std::cout << " failed" << std::endl;
	
	///////////////////////////////////////
	// Optimize Params of Proj. Ladmarks //
	///////////////////////////////////////
	Landmarks::Params paramsProj(paramsAvg);
	Landmarks::optimizeParams(baseParams, paramsProj, landmarksProj);

	////////////////////////
	////// Save Dense //////
	////////////////////////
	filepath.str("");
	filepath << outputPath << "dense.ply";
	Landmarks::createMorphedPoints(paramsProj, baseParamsMesh, true, filepath.str());
	Landmarks::saveParams(outputPath, paramsProj, "final");

	return EXIT_SUCCESS;
}